<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactValidate;
use Fynduck\FilesUpload\PrepareFile;
use Illuminate\Support\Str;

class ContactController extends Controller
{
    public function sendContactForm(ContactValidate $request)
    {
        $message = ['message' => 'form send success'];

        $nameCv = PrepareFile::uploadFile('cv', 'file', base64_decode($request->get('cv')), null, Str::slug($request->get('first_name') . ' ' . $request->get('last_name')), [], $request->get('extensionCv'));

        $create = Contact::create([
            'first_name' => $request->get('first_name'),
            'last_name'  => $request->get('last_name'),
            'birthday'   => $request->get('birthday'),
            'address'    => $request->get('address'),
            'cv'         => $nameCv,
        ]);

        if (!$create)
            $message = ['message' => 'form send success'];

        return response()->json($message);
    }
}
