<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
    </head>
    <body>
        <main id="app" class="container my-5">
            <cv-form source="{{ route('send-contact') }}"></cv-form>
        </main>

        <script src="{{ asset(mix('js/app.js')) }}"></script>
    </body>
</html>
